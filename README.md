# VARTISTE Extras

This repository serves as a git-lfs store for some of the large assets for [VARTISTE](https://gitlab.com/zach-geek/vartiste) (the default VARTISTE extras), as well as the repository for running your own vartiste-extras-server.

## VARTISTE Extras Server

If you'd like an easy way to get data into VARTISTE, especially if it's running
on a mobile VR headset, you can run your own vartiste-extras server.

You can either just serve static files, or run the server included with this repo.

If you want to serve static files, they should follow the layout of the [extras folder](https://gitlab.com/zach-geek/vartiste-extras/-/tree/main/extras), and all the files should be listed in an [index.json](https://gitlab.com/zach-geek/vartiste-extras/-/blob/main/extras/index.json) file.

**Server Note:** When serving these files, the server must be set up to serve CORS headers. Likely you'll be running VARTISTE from `https://vartiste.xyz`, so you'll want to add that as an allowed origin. Or if you're running a vartiste development server, you'll want to add localhost. In fact, it may be easiest to allow all origins (`*`). _Additionally_, if you are not running the server on `localhost`, then you must also serve it over `https`. This is due to built-in browser security mechanisms regarding mixed-origin content.

If you don't want to manually maintain the index.json file, you can run the [vartiste-extras](https://www.npmjs.com/package/vartiste-extras) command:

  1. First, install it globally using npm: `npm i -g vartiste-extras`
  2. Then set up folders for the different categories (all folders optional), and populate them with the files you want to make available to VARTISTE:
    - `reference-images`
    - `reference-models`
    - `material-packs`
    - `brush-packs`
    - `hdris`
    - `projects`
  3. Then in the root folder containing the other folders, run the command: `vartiste-extras [COMMAND]` (Run it without any options to see available commands. Unfortunately it is not perfectly simple to set up.)
  5. It will start serving the files and automatically generate the index.json file

## Adding Extras to VARTISTE

Once you've got the extras served somewhere, you can add them to VARTISTE through the Extras Shelf, in the Load Shelf, in the Project / Settings Shelf:

![Adding Extras](addextras.jpg)

Enter the base URL where the extras are being served. Then use the Load Extras
button categories to load them into your project.

## License

The source code in this repository is licensed under the MIT license.

The assets in this repository are either in the public domain, or licensed under the CC0 license.
